﻿using NUnit.Framework;
using polish_notation_withTrees;
using System;
using System.Collections.Generic;
using System.Collections.Immutable;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using funkylib;
using funkylib.ValueTypes;

namespace polish_notation_withTrees.Tests {
  [TestFixture()]
  public class ProgramTests {
    [Test()]
    public void validateSimple() {
      var test = "1 + 3 + 4 + 5 + 6 + 6 + 6 + 6 + 6 + 6";
      var tokenized = Program.tokenize(test, ' ').run();
      var actual = tokenized.fold(
        exception => Option.None,
        tokensToParse => Program.parseTokens(tokensToParse, tokenToParse => Program.parseToken(tokenToParse, Int.parse))
      );
      Assert.True(actual.isSome);
    }
    [Test()]
    public void validateFalse() {
      var test = "1 + 3 + 4 + 5 + 6 + 6 + 6 a + 6 + 6 + 6";
      var tokenized = Program.tokenize(test, ' ').run();
      var actual = tokenized.fold(
        exception => Option.None,
        tokensToParse => Program.parseTokens(tokensToParse, tokenToParse => Program.parseToken(tokenToParse, Int.parse))
      );
      Assert.False(actual.isSome);
    }

    [Test()]
    public void parseFail() {
      var input = "+ 3 4 5";
      var tokenized = Program.tokenize(input, ' ').run();
      var tokensOpt = tokenized.fold(
        exception => Option.None,
        tokensToParse => Program.parseTokens(tokensToParse, Int.parse)
      );
      if(tokensOpt.isNone)
        throw new Exception();
      var tokens = tokensOpt._unsafe;
      var actual = Program.calculate(tokens);
    Assert.True(actual.isLeft);
    }

    [Test()]
    public void parseTrue() {
      var input = "+ 3 4";
      var tokenized = Program.tokenize(input, ' ').run();
      var tokensOpt = tokenized.fold(
        exception => Option.None,
        tokensToParse => Program.parseTokens(tokensToParse, Int.parse)
      );
      if (tokensOpt.isNone)
        throw new Exception();
      var tokens = tokensOpt._unsafe;
      var actual = Program.parse(tokens);
      Assert.True(!actual.isLeft);
    }
  }
}
﻿using System;
using System.Collections.Generic;
using System.Collections.Immutable;
using System.Linq;
using funkylib;
using Double = funkylib.ValueTypes.Double;

namespace polish_notation_withTrees {
  public static class Program {

    public static IO<Unit> println(string s) => new IO<Unit>(() => {
      Console.WriteLine(s);
      return new Unit();
    });

    public static IO<string> readln() => new IO<string>(Console.ReadLine);

    public static IO<Unit> mainIO(ImmutableList<string> args) =>
      readln().flatMap(input => 
        tokenize(input, ' ').run().fold(
          exception => println(exception.Message),
          tokensToParse => printExpression(isRPN<double>(tokensToParse), tokensToParse, Double.parse)
        )
      );

    public static IO<Unit> printExpression<A>(bool isReversePolishNotation, string[] tokensToParse, Func<string, Option<A>> valueParser) {
      if (isReversePolishNotation)
        tokensToParse = convertRPNtoPN(tokensToParse);
      return parseTokens(tokensToParse, Double.parse).fold(
        () => println("Invalid expression"),
        tokens => parse(tokens).fold(
          println,
          result => {
            var exp = result.expression;
            return println(isReversePolishNotation
              ? $"{exp.reverse().toInfix()} = {exp.reverse().evaluate(doubleNumeric.instance)}"
              : $"{exp.toInfix()} = {exp.evaluate(doubleNumeric.instance)}");
          }
        )
      );
    }

    static bool isRPN<A>(string[] tokens) => 
      _Operator<A>.parse(tokens[0]).isNone;

    static string[] convertRPNtoPN(IEnumerable<string> tokensToConvert) =>
      tokensToConvert.Reverse().ToArray();

    static void Main(string[] args) {
      while (true) {
        mainIO(args.ToImmutableList()).unsafeRun();
      }
    }

    public static Try<string[]> tokenize(string s, char separator) => () => s.Split(separator);

    public static Option<ImmutableList<IToken<A>>> parseTokens<A>(string[] tokens, Func<string, Option<A>> valueParser) {
      var parsedTokens = new List<IToken<A>>();
      foreach (var token in tokens) {
        var tokenOpt = parseToken(token, valueParser);
        if (tokenOpt.isNone)
          return Option.None;
        else
          parsedTokens.Add(tokenOpt._unsafe);
      }
      return parsedTokens.ToImmutableList().some();
    }

    public static Option<IToken<A>> parseToken<A>(string token, Func<string, Option<A>> valueParser) =>
      valueParser(token).fold(
        () => _Operator<A>.parse(token).fold<Option<IToken<A>>>(
          () => Option.None,
          @operator => new _Operator<A>(@operator)
        ),
        value => new Lit<A>(value)
      );

    static Either<string, IToken<A>> get<A>(ImmutableList<IToken<A>> tokens, int index) =>
      tokens.get(index).fold<Either<string, IToken<A>>>(
        () => $"Couldn't access {index} token",
        token => token.right()
      );

    public static Either<string, Result<A>> parse<A>(ImmutableList<IToken<A>> restTokens) => 
      calculate(restTokens).fold<Either<string, Result<A>>>(
        err => err,
        result => {
          if (result.unparsedTokens.IsEmpty)
            return result;
          else
            return $"Unexpected expression end";
        }
    );

    public static Either<string, Result<A>> calculate<A>(ImmutableList<IToken<A>> restTokens) {
      var firstTokenEither = get(restTokens, 0);
      if (firstTokenEither.isLeft)
        return firstTokenEither.__unsafeGetLeft;
      var firstToken = firstTokenEither.__unsafeGetRight;

      restTokens = restTokens.Skip(1).ToImmutableList();

      return firstToken.match(
        @operator => calculate(restTokens).flatMap(result1 => 
          calculate(result1.unparsedTokens).map(
            err => err,
            result2 => new Result<A>(
              new Op<A>(result1.expression, result2.expression, @operator.value), result2.unparsedTokens
            )
          )
        ),
        lit => new Result<A>(new Lit<A>(lit.a), restTokens)
      );
    }
  }

  public class intNumeric : Numeric<int> {
    public static Numeric<int> instance = new intNumeric();
    intNumeric() { }

    public int add(int a1, int a2) => a1 + a2;
    public int sub(int a1, int a2) => a1 - a2;
    public int mult(int a1, int a2) => a1 * a2;
    public int div(int a1, int a2) => a1 / a2;
  }

  public class doubleNumeric : Numeric<double> {
    public static Numeric<double> instance = new doubleNumeric();
    doubleNumeric() { }

    public double add(double a1, double a2) => a1 + a2;
    public double sub(double a1, double a2) => a1 - a2;
    public double mult(double a1, double a2) => a1 * a2;
    public double div(double a1, double a2) => a1 / a2;
  }

  public struct Result<A> {
    public readonly IExpression<A> expression;
    public readonly ImmutableList<IToken<A>> unparsedTokens;

    public Result(IExpression<A> expression, ImmutableList<IToken<A>> unparsedTokens) {
      this.expression = expression;
      this.unparsedTokens = unparsedTokens;
    }
  }
}

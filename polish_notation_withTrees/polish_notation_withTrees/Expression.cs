﻿using System;
using funkylib;

namespace polish_notation_withTrees {
  public interface Numeric<A> {
    A add(A a1, A a2);
    A sub(A a1, A a2);
    A mult(A a1, A a2);
    A div(A a1, A a2);
  }

  public interface IExpression<A> {
    A evaluate(Numeric<A> num);
    string toInfix();
    IExpression<A> reverse();
  }

  public sealed class Lit<A> : IExpression<A>, IToken<A>{
    public readonly A a;

    public Lit(A a) { this.a = a; }
    public A evaluate(Numeric<A> num) => a;
    public string toInfix() => a.ToString();
    public IExpression<A> reverse() => this;

    public B match<B>(Func<_Operator<A>, B> matchOp, Func<Lit<A>, B> matchLit) => matchLit(this);
  }

  public enum Operator  { Plus, Minus, Mult, Div }
  public sealed class Op<A> : IExpression<A> {
    public readonly IExpression<A> left, right;
    public readonly Operator op;

    public Op(IExpression<A> left, IExpression<A> right, Operator op) {
      this.left = left;
      this.right = right;
      this.op = op;
    }

    public IExpression<A> reverse() => new Op<A>(right.reverse(), left.reverse(), op);

    public A evaluate(Numeric<A> num) {
      switch (op) {
        case Operator.Plus:
          return num.add(left.evaluate(num), right.evaluate(num));
        case Operator.Minus:
          return num.sub(left.evaluate(num), right.evaluate(num));
        case Operator.Mult:
          return num.mult(left.evaluate(num), right.evaluate(num));
        case Operator.Div:
          return num.div(left.evaluate(num), right.evaluate(num));
      }
      throw new Exception("Couldn't evaluate expression");
    }

    static char operatorToSymbol(Operator op) {
      switch (op) {
        case Operator.Plus:
          return '+';
        case Operator.Minus:
          return '-'; 
        case Operator.Mult:
          return '*';
        case Operator.Div:
          return '/';
      }
      throw new Exception("Operator not supported");
    }

    public string toInfix() {
      var @operator = operatorToSymbol(op);
      return $"({left.toInfix()} {@operator} {right.toInfix()})";
    }

  }
  public interface IToken<A> {
    B match<B>(Func<_Operator<A>, B> matchOp, Func<Lit<A>, B> matchLit);
  }

  public class _Operator<A> : IToken<A> {
    public readonly Operator value;
    public _Operator(Operator value) { this.value = value; }
    public static Option<Operator> parse(string s) {
      switch (s) {
        case "+":
          return Operator.Plus;
        case "-":
          return Operator.Minus;
        case "*":
          return Operator.Mult;
        case "/":
          return Operator.Div;
      }
      return Option.None;
    }

    public B match<B>(Func<_Operator<A>, B> matchOp, Func<Lit<A>, B> matchLit) => matchOp(this);
  }
}
﻿//using System;
//using System.Collections.Immutable;
//using System.ComponentModel;
//using System.Linq;
//using funkylib;
//
//namespace PN_RPN {
//  public abstract class PolishNotation<A> : Operators<A> {
//    public abstract A sum(A left, A right);
//    public abstract A divide(A left, A right);
//    public abstract A multiply(A left, A right);
//    public abstract A subtract(A left, A right);
//
//    Try<string[]> tokenize(string s, char separator) => () => s.Trim().Split(separator);
//
//    public Either<string, A> getResult(string s, char separator) =>
//      tokenize(s, separator).run().fold(
//        exception => exception.Message,
//        parts => { }
//      );
//
//    public string toInfixWithResult(string s, char separator) {
//      var expression = toInfix(s, separator);
//      var result = getResult(s, separator);
//      return expression.fold(
//        () => "Expression cannot be expressed as infix notation",
//        expr => {
//          var res = result.fold(
//            err => err,
//            value => value.ToString()
//          );
//          return $"{expr} = {res}";
//        }
//      );
//    }
//
//    public Option<string> toInfix(string s, char separator) {
//      var infixStack = ImmutableStack<string>.Empty;
//      return tokenize(s, separator).run().fold(
//        exception => Option.None,
//        parts => {
//          if (parts.Length < 3)
//            return Option.None;
//          else {
////            foreach (var toParse in parts) {
////              var parsedOpt = pushOperand(infixStack, toParse).orElse(() => applyInfixOperator(infixStack, toParse));
////              if (parsedOpt.isSome)
////                infixStack = parsedOpt._unsafe;
////              else
////                return Option.None;
////            }
//          }
//          string value;
//          infixStack.Pop(out value);
//          return value.some();
//        }
//      );
//    }
//
//    Option<Tuple<string, ImmutableStack<string>>> operatorToInfix(ImmutableStack<string> infixStack, string @operator) {
//      string left, right;
//      var poppedStack = infixStack.Pop(out right).Pop(out left);
//      switch (@operator) {
//        case "+":
//          return new Tuple<string, ImmutableStack<string>>($"({left} + {right})", poppedStack);
//        case "-":
//          return new Tuple<string, ImmutableStack<string>>($"({left} - {right})", poppedStack);
//        case "*":
//          return new Tuple<string, ImmutableStack<string>>($"({left} * {right})", poppedStack);
//        case "/": {
//          return new Tuple<string, ImmutableStack<string>>($"({left} / {right})", poppedStack);
//        }
//      }
//      return Option.None;
//    }
//
//    Either<string, A> calculate(string[] toBeParsed) {
//      if (toBeParsed.Length < 3)
//        return "Not enough members in expression";
//      var operationalStack = ImmutableStack<Func<A>>.Empty;
//      
//      foreach (var toParse in toBeParsed.Skip(3)) {
//        
//      }
//
//    }
//
//    public Either<string, Func<A>> applyOperatorLazy(A left, A right, string @operator) {
//      switch (@operator) {
//        case "+":
//          return (Func<A>)(() => sum(left, right));
//        case "-":
//          return (Func<A>)(() => subtract(left, right));
//        case "*":
//          return (Func<A>)(() => multiply(left, right));
//        case "/": {
//          return (Func<A>)(() => divide(left, right));
//        }
//      }
//
//      return $"Operand with signature {@operator} doesn't exist";
//    }
//
//    public static Either<string, ImmutableStack<Func<A>>> pushOperator(ImmutableStack<Func<A>> operationalStack, string @operator) {
//      Func<A> left, right;
//      var poppedStack = operationalStack.Pop(out right).Pop(out left);
//
//    }
//
//
//    public static Either<string, ImmutableStack<A>> pushOperand(ImmutableStack<A> operationalStack, string operand) =>
//      parseOperand(operand).map(err => err, operationalStack.Push);
//
//    public static Option<ImmutableStack<string>> pushOperand(ImmutableStack<string> infixStack, string operand) =>
//      parseOperand(operand).fold<Option<ImmutableStack<string>>>(
//        err => Option.None,
//        a => infixStack.Push(a.ToString())
//      );
//
//    Option<ImmutableStack<string>> applyInfixOperator(ImmutableStack<string> infixStack, string @operator) =>
//      operatorToInfix(infixStack, @operator).map(tuple => tuple.Item2.Push(tuple.Item1));
//
//    public static Either<string, A> parseOperand(string operand) {
//      try {
//        var converter = TypeDescriptor.GetConverter(typeof(A));
//        return (A)converter.ConvertFromString(operand);
//      }
//      catch (Exception ex) {
//        return ex.Message;
//      }
//    }
//  }
//}
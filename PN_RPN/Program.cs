﻿using System;

namespace PN_RPN {
  class Program {
    static void Main(string[] args) {
      var parser = ReversePolishNotationDouble.parser;
      Console.WriteLine(parser.toInfixWithResult("3 5 4 / 20 * 21 3 * - + 5 +", ' '));

      string line;
      while ((line = Console.ReadLine()) != null) {
        try { Console.WriteLine(parser.toInfixWithResult(line, ' ')); }
        catch (Exception e) { Console.WriteLine(e); }
      }
    }
  }
}

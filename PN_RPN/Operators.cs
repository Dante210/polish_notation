﻿namespace PN_RPN {
  public interface Operators<A> {
    A sum(A left, A right);
    A divide(A left, A right);
    A multiply(A left, A right);
    A subtract(A left, A right);
  }
}
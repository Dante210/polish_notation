﻿namespace PN_RPN {
  public class RPNotationInt : ReversePolishNotation<int> {
    public override int sum(int left, int right) => left + right;
    public override int divide(int left, int right) => left / right;
    public override int multiply(int left, int right) => left * right;
    public override int subtract(int left, int right) => left - right;
  }

  public struct ReversePolishNotationInt {
    public static readonly RPNotationInt parser = new RPNotationInt(); 
  }

  public class RPNotationDouble : ReversePolishNotation<double> {
    public override double sum(double left, double right) => left + right;
    public override double divide(double left, double right) => left / right;
    public override double multiply(double left, double right) => left * right;
    public override double subtract(double left, double right) => left - right;
  }

  public struct ReversePolishNotationDouble {
    public static readonly RPNotationDouble parser = new RPNotationDouble();
  }
}